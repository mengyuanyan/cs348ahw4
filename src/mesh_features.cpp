#include "mesh_features.h"
using namespace OpenMesh;

bool isSilhouette(Mesh &mesh, const Mesh::EdgeHandle &e, Vec3f cameraPos)  {
  // CHECK IF e IS A SILHOUETTE HERE -----------------------------------------------------------------------------
    const Mesh::HalfedgeHandle heh_0 = mesh.halfedge_handle(e, 0);
    const Mesh::HalfedgeHandle heh_1 = mesh.halfedge_handle(e, 1);

    const Mesh::FaceHandle fh_0 = mesh.face_handle(heh_0);
    const Vec3f c0 = mesh.calc_face_centroid(fh_0);
    const Mesh::FaceHandle fh_1 = mesh.face_handle(heh_1);
    const Vec3f c1 = mesh.calc_face_centroid(fh_1);
    Vec3f v0 = cameraPos - c0;
    double dir_0 = dot(v0, mesh.normal(fh_0));
    Vec3f v1 = cameraPos - c1;
    double dir_1 = dot(v1, mesh.normal(fh_1));
    if (dir_0 * dir_1 <= 0) {
      return true;
    } else {
      return false;
    }

  // -------------------------------------------------------------------------------------------------------------
}

bool isSharpEdge(Mesh &mesh, const Mesh::EdgeHandle &e) {
  // CHECK IF e IS SHARP HERE ------------------------------------------------------------------------------------
    const Mesh::HalfedgeHandle heh_0 = mesh.halfedge_handle(e, 0);
    const Mesh::HalfedgeHandle heh_1 = mesh.halfedge_handle(e, 1);

    const Mesh::FaceHandle fh_0 = mesh.face_handle(heh_0);
    const Mesh::FaceHandle fh_1 = mesh.face_handle(heh_1);
    double angle = dot(mesh.normal(fh_0), mesh.normal(fh_1));
    if (angle < 0.5) {
      return true;
    } else {
      return false;
    }

  // -------------------------------------------------------------------------------------------------------------
}

bool isFeatureEdge(Mesh &mesh, const Mesh::EdgeHandle &e, Vec3f cameraPos) {
  return mesh.is_boundary(e) || isSilhouette(mesh, e, cameraPos) || isSharpEdge(mesh, e);
}
