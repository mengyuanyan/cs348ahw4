#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <iostream>
#include "curvature.h"

using namespace OpenMesh;
using namespace Eigen;
using namespace std;

void computeCurvature(Mesh& mesh, OpenMesh::VPropHandleT<CurvatureInfo>& curvature) {
  // WRITE CODE HERE TO COMPUTE THE CURVATURE AT THE CURRENT VERTEX ----------------------------------------------
  for (Mesh::VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
    // WRITE CODE HERE TO COMPUTE THE CURVATURE AT THE CURRENT VERTEX ----------------------------------------------
    const Mesh::VertexHandle vh = (*v_it);
    const Vec3f normal = mesh.normal(vh);
    const Vector3d n(normal[0], normal[1], normal[2]); // example of converting to Eigen's vector class for easier math
    std::vector<Vector3d> edges;
    for (Mesh::VertexVertexIter vv_it = mesh.vv_iter(vh); vv_it.is_valid(); ++vv_it) {
      const Mesh::VertexHandle vn = (*vv_it);
      Vec3f edge = mesh.point(vh) - mesh.point(vn);
      Vector3d edge_e(edge[0], edge[1], edge[2]);
      edges.push_back(edge_e);
    }
    std::vector<float> faceAreas;
    for (int i = 0; i < edges.size(); i++){
      Vector3d area;
      if (i == edges.size() - 1) {
        area = edges[i].cross(edges[0]);
      } else {
        area = edges[i].cross(edges[i+1]);
      }
      faceAreas.push_back(area.norm());
    }
    // In the end you need to fill in this struct
    Matrix3d projection = Matrix3d::Identity();
    projection = projection - n*n.transpose();
    Matrix3d M = Matrix3d::Zero();
    for (int i = 0; i < edges.size(); i++){
      Vector3d T = projection * edges[i];
      T = T / T.norm();
      float kappa = 2 * n.transpose() * edges[i];
      kappa = kappa / edges[i].squaredNorm();
      float weight = faceAreas[i];
      if (i == 0) {
        weight += faceAreas[faceAreas.size()-1];
      } else {
        weight += faceAreas[i-1];
      }
      M = M + weight * kappa * T * T.transpose();
    }

    EigenSolver<Matrix3d> eigs(M);
    Vector3d eigvalue = eigs.eigenvalues().real();
    Vector3d eigvalueabs = eigvalue.cwiseAbs();
    Matrix3d eigvector = eigs.eigenvectors().real();
    double minvalue = eigvalueabs.minCoeff();
    int idx1 = -1, idx2 = -1;
    for (int i = 0; i < 3; i++){
      if (eigvalueabs(i) != minvalue) {
        if (idx1 != -1) {
          idx2 = i;
        } else {
          idx1 = i;
        }
      }
    }
    CurvatureInfo info;
    info.curvatures[0] = eigvalue(idx1);
    info.curvatures[1] = eigvalue(idx2);
    info.directions[0] = Vec3f(eigvector(0,idx1), eigvector(1,idx1), eigvector(2,idx1));
    info.directions[1] = Vec3f(eigvector(0,idx2), eigvector(1,idx2), eigvector(2,idx2));
    mesh.property(curvature, vh) = info;
  }
  // -------------------------------------------------------------------------------------------------------------
}

void computeViewCurvature(Mesh& mesh, OpenMesh::Vec3f camPos, OpenMesh::VPropHandleT<CurvatureInfo>& curvature,
  OpenMesh::VPropHandleT<double>& viewCurvature, OpenMesh::FPropHandleT<OpenMesh::Vec3f>& viewCurvatureDerivative) {
  // WRITE CODE HERE TO COMPUTE CURVATURE IN THE VIEW PROJECTION PROJECTED ON THE TANGENT PLANE ------------------
  // Compute vector to viewer and project onto tangent plane, then use components in principal directions to find curvature
  // -------------------------------------------------------------------------------------------------------------
  for (Mesh::ConstVertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it) {
    const Mesh::VertexHandle vh = (*v_it);
    Vec3f p = camPos - mesh.point(vh);
    Vec3f n = mesh.normal(vh);
    double normalProj = OpenMesh::dot(p, n);
    Vec3f projection = p - normalProj * n;
    
    CurvatureInfo curv = mesh.property(curvature, vh);
    double cosine = OpenMesh::dot(projection, curv.directions[0]) / projection.norm();
    double sine = OpenMesh::dot(projection, curv.directions[1]) / projection.norm();
    double viewCurv = curv.curvatures[0] * cosine * cosine + curv.curvatures[1] * sine * sine;
    mesh.property(viewCurvature, vh) = viewCurv;
  }
  // We'll use the finite elements piecewise hat method to find per-face gradients of the view curvature
  // CS 348a doesn't cover how to differentiate functions on a mesh (Take CS 468!) so we provide code here

  for (Mesh::ConstFaceIter f_it = mesh.faces_begin(); f_it != mesh.faces_end(); ++f_it) {
    const Mesh::FaceHandle fh = (*f_it);

    Vec3f p[3];
    double c[3];
    Mesh::ConstFaceVertexIter fv_it = mesh.cfv_begin(fh);
    for (int i = 0; i < 3 && fv_it != mesh.cfv_end(fh); ++i, ++fv_it) {
      const Mesh::VertexHandle n_vh = (*fv_it);
      p[i] = mesh.point(n_vh);
      c[i] = mesh.property(viewCurvature, n_vh);
    }

    const Vec3f n = mesh.normal(fh);
    double area = mesh.calc_sector_area(mesh.halfedge_handle(fh));

    mesh.property(viewCurvatureDerivative, fh) =
      cross(n, (p[0] - p[2])) * (c[1] - c[0]) / (2 * area) +
      cross(n, (p[1] - p[0])) * (c[2] - c[0]) / (2 * area);
  }
}
